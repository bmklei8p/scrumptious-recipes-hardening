from django import template

register = template.Library()


def resize_to(ingredient, num):
    if num is not None and ingredient.recipe.servings is not None:
        alter_by = int(num) / int(ingredient.recipe.servings)
        result = ingredient.amount * alter_by
        return result
    return ingredient.amount


register.filter(resize_to)
