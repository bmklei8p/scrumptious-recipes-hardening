from django.urls import path

from meal_plans.views import (
    Meal_planCreateView,
    Meal_planListView,
    Meal_planDetailView,
    Meal_planDeleteView,
    Meal_planUpdateView,
)

urlpatterns = [
    path("", Meal_planListView.as_view(), name="meal_plans_list"),
    path(
        "create/",
        Meal_planCreateView.as_view(),
        name="meal_plan_create",
    ),
    path(
        "<int:pk>/",
        Meal_planDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "<int:pk>/edit/",
        Meal_planUpdateView.as_view(),
        name="meal_plan_edit",
    ),
    path(
        "<int:pk>/delete/",
        Meal_planDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
